import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import mongoose from 'mongoose'
import dotenv from 'dotenv'

const app = express();
const port = 8000; // default port to listen

app.use(helmet())
app.use(cors())

// parsing req.body to json
app.use(express.json())

// using dotenv
const env = dotenv.config()
console.log('env: ', env);

//DB
mongoose.connect(process.env.MONGODB_CONNECTION_STRING, {useNewUrlParser: true, useUnifiedTopology: true});

//Get the default connection
const db = mongoose.connection;

const todoSchema = new mongoose.Schema({
    title: String,
    order: Number,
    completed: { type: Boolean, default: false},
    url: { type: String, default: ''}
});

// Compile model from schema
const todoModel = mongoose.model('todos', todoSchema );

app.get('/:id', async (req, res) => {
    const { id } = req.params

    const todo = await todoModel.findOne({
        _id: id
    })

    res.send(todo)
})

app.patch('/:id', async (req, res) => {
    const body = req.body
    const { id } = req.params

    const todo = await todoModel.findByIdAndUpdate({
        _id: id
    }, {
        $set: body
    }, {
        new: true
    })
    console.log('todo: ', todo);

    res.send(todo)
})

app.delete('/:id', async (req, res) => {
    const body = req.body
    const { id } = req.params

    const todo = await todoModel.findByIdAndDelete({
        _id: id
    })
    console.log('todo: ', todo);

    res.send(todo)
})

app.get( "/", async ( req, res ) => {
    const todos = await todoModel.find({})
    res.send(todos)
} );

app.post('/', async (req, res) => {
    const body = req.body
    console.log('body: ', body);

    const inserted = await todoModel.create(body)

    const updatedResult = await todoModel.findByIdAndUpdate({
        _id: inserted._id
    }, {
        $set: {
            url: `${process.env.BASE_URL}/${inserted._id}`
        }
    },
    {
        new: true
    })

    res.send(updatedResult)
})

app.delete('/', async (req, res) => {
    const deleteResult = await todoModel.deleteMany({})
    console.log('deleteResult: ', deleteResult);
    res.send([])
})

// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );